using UnityEngine;
/// <summary>
/// Allows for the application of Newtonian gravity on the objct this script is attached to towards every object tagged "Planet".
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class NewtonianGravity : MonoBehaviour
{
    #region Variables
    #region Private
    /// <summary>
    /// The ridged body component of the object the script is attached to.
    /// </summary>
    private Rigidbody2D _rigidbody2D;
    /// <summary>
    /// All objects that affect the gravitational pull of this object.
    /// </summary>
    private GameObject[] _planets;
    /// <summary>
    /// A structure to contain planets and the forces they are applying on the players.
    /// </summary>
    private struct AsteralBodyForce
    {
        public AsteralBodyForce(float a_force, GameObject a_asteralBody)
        {
            force = a_force;
            asteralBody = a_asteralBody;
        }
        public float force;
        public GameObject asteralBody;
    };
    #endregion
    #region Editor
    /// <summary>
    /// The speed at which the object rotates towards the ground.
    /// </summary>
    [SerializeField] private float rotateSpeed = 0.25f;
    #endregion
    #endregion
    #region Methods
    #region Unity
    /// <summary>
    /// Populates the planets array and retrieves the rigid body.
    /// </summary>
    private void Start()
    {
        _planets = GameObject.FindGameObjectsWithTag("Planet");
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }
    /// <summary>
    /// Apply forces each physics frame per second (keeps things in sync with the physics engine itself).
    /// </summary>
    private void FixedUpdate()
    {
        // The most attractive planet.
        AsteralBodyForce currentLargestPlanet = new AsteralBodyForce(0.0f, null);

        // Apply gravity towards every planet in the array.
        foreach (GameObject Object in _planets)
        {
            // Stops self interaction
            if (gameObject == Object)
                continue;
            
            // Apply the force to the object towards the planets.
            float force = ApplyGravity(GetComponent<Rigidbody2D>(), Object.GetComponent<Rigidbody2D>());

            // Find the most attractive planet.
            if (currentLargestPlanet.asteralBody == null || force > currentLargestPlanet.force)
                currentLargestPlanet = new AsteralBodyForce(force, Object);
        }

        // Make the player face the most attractive planet.
        // Gets a vector that points from the player's position to the target's.
        Vector3 directionToPlanet = currentLargestPlanet.asteralBody.transform.position - transform.position;
        // Sets the rotation that the entity needs to be to look like its on the planet
        Quaternion rotation = Quaternion.LookRotation(Vector3.forward,-directionToPlanet);
        // Lerps the objects rotations to turn it to face the ground.
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotateSpeed);
    }
    #endregion
    #region Private
    /// <summary>
    /// Moves A towards B depending on distance between them and the mass of each object.
    /// </summary>
    /// <param name="a">The object that had force applied to</param>
    /// <param name="b">the object causing the force</param>
    private float ApplyGravity(Rigidbody2D a, Rigidbody2D b)
    {
        // Get distance between both objects.
        Vector3 distanceVector = b.transform.position - a.transform.position;
        float distanceScalar = distanceVector.magnitude;
        distanceVector /= distanceScalar;

        // This is the Newton's equation.
        // G = 6.67 * 10^-11 N.m�.kg^-2
        double Gravity = 6.674f * (10 ^ 11);
        float force = ((float)Gravity * a.mass * b.mass) / (distanceScalar * distanceScalar);

        // Application of force to object.
        a.AddForce(distanceVector * force);

        // Returns force as to compare it to other planets so that the player can be rotated the correct direction.
        return force;
    }
    #endregion
    #endregion
}