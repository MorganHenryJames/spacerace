﻿using UnityEngine;
/// <summary>
/// Gets the player input for movement and sends it off to the character controllers.
/// </summary>
[RequireComponent(typeof(CharacterController2D))]
public class PlayerMovementInput : MonoBehaviour
{
    #region Variables
    #region Private
    /// <summary>
    /// The character controller for the player that applies the movement.
    /// </summary>
    private CharacterController2D _characterController2D;
    /// <summary>
    /// Positive when moving right and negative when moving left.
    /// </summary>
    private float _horizontalMovement;
    /// <summary>
    /// True when the next frame should apply a jump calculation.
    /// </summary>
    private bool _jump = false;
    #endregion
    #region Editor
    /// <summary>
    /// How fast the user can move.
    /// </summary>
    [SerializeField] private float _runSpeed = 300.0f;
    #endregion
    #endregion
    #region Methods
    #region Unity
    /// <summary>
    /// Sets the rigid body and landing event.
    /// </summary>
    private void Awake()
    {
        _characterController2D = GetComponent<CharacterController2D>();
    }
    /// <summary>
    /// Gets the input of the user each frame.
    /// </summary>
    private void Update()
    {
        _horizontalMovement = Input.GetAxisRaw("Horizontal") * _runSpeed;

        if (Input.GetButtonDown("Jump"))
            _jump = true;
    }
    /// <summary>
    /// Tells the character controller what movement if any has been initiated.
    /// </summary>
    private void FixedUpdate()
    {
        _characterController2D.Move(_horizontalMovement * Time.fixedDeltaTime, _jump);
        _jump = false;
    }
    #endregion
    #endregion
}