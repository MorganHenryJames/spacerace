﻿using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Houses physics calculations for the player.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class CharacterController2D : MonoBehaviour
{
    #region Variables
    #region Editor
    /// <summary>
    /// Amount of force added when the player jumps.
    /// </summary>
    [SerializeField] private float _jumpForce = 400.0f;
    /// <summary>
    /// Whether or not a player can steer while jumping.
    /// </summary>
    [SerializeField] private bool _airControl = false;
    /// <summary>
    /// A mask determining what is ground to the character.
    /// </summary>
    [SerializeField] private LayerMask _whatIsGround;                          
    /// <summary>
    /// A position marking where to check if the player is grounded. 
    /// </summary>
    [SerializeField] private Transform _groundCheck;                          
    #endregion
    #region Private
    /// <summary>
    /// The ridged body component of the object the script is attached to.
    /// </summary>
    private Rigidbody2D _rigidbody2D;
    /// <summary>
    /// Radius of the overlap circle to determine if grounded.
    /// </summary>
    private const float _groundedRadius = .2f;
    /// <summary>
    /// Whether or not the player is grounded.
    /// </summary>
    private bool _grounded;            
    /// <summary>
    /// Radius of the overlap circle to determine if the player can stand up.
    /// </summary>
    private const float _ceilingRadius = .2f; 
    /// <summary>
    /// For determining which way the player is currently facing.
    /// </summary>
    private bool _facingRight = true;  
    #endregion
    #region Public 
    [HideInInspector]
    /// <summary>
    /// Occurs when the player has landed for use of animation and whatnot.
    /// </summary>
    public UnityEvent OnLandEvent;
    #endregion
    #endregion
    #region Methods
    #region Unity
    /// <summary>
    /// Sets the rigid body and landing event.
    /// </summary>
    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();
    }
    /// <summary>
    /// Moves the player depending on input.
    /// </summary>
    private void FixedUpdate()
    {
        bool wasGrounded = _grounded;
        _grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground.
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(_groundCheck.position, _groundedRadius, _whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                _grounded = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }
    }
    #endregion
    #region Public
    /// <summary>
    /// Applies velocities to the player depending on input and also flip the character to face the correct direction.
    /// </summary>
    /// <param name="a_move">The force to add horizontally.</param>
    /// <param name="a_jump">The force to add vertically.</param>
    public void Move(float a_move, bool a_jump)
    {
        // Only control the player if grounded or airControl is turned on.
        if (_grounded || _airControl)
        {
            // Apply velocity depending on the input.
            _rigidbody2D.velocity += (Vector2)(transform.right * a_move);

            // If the input is moving the player right and the player is facing left...
            if (a_move > 0 && !_facingRight)
                // ... flip the player.
                Flip();
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (a_move < 0 && _facingRight)
                // ... flip the player.
                Flip();
        }
        // If the player should jump...
        if (_grounded && a_jump)
        {
            // Add a vertical force to the player.
            _grounded = false;

            _rigidbody2D.velocity += (Vector2)(transform.up * _jumpForce);
        }
    }
    #endregion
    #region Private
    /// <summary>
    /// Flips the player to face the correct direction.
    /// </summary>
    private void Flip()
    {
        // Switch the way the player is labeled as facing.
        _facingRight = !_facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    #endregion
    #endregion
}
