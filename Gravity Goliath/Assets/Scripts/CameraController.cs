﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//stage two zoom out and in to fit the amount of players
//stage three spectate mode to find centre and also all players
//stage four function to swap player following - along with going into spectate

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Vector3 offsetPosition;
    private float smoothTime = 5.0f;
    
    private void LateUpdate()
    {
        Refresh();
    }

    public void Refresh()
    {
        transform.position = Vector3.Lerp(transform.position, target.position + offsetPosition, smoothTime * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, smoothTime * Time.deltaTime);
    }
}